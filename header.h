#pragma once
#include <iostream>	
#include <fstream>
#include <string>
using namespace std;

struct Node // Mot node luu mot don thuc
{
	float parameters; // He so
	char* variable; // Bien so
	int* index_number; // So mu
	Node* pNext;
};

class Polynomial // Luu mot da thuc
{
private:
	Node *pHead;
public:
	Polynomial();
	Polynomial& operator=(Polynomial a);
	Polynomial add2Polynomial(Polynomial a);
	Polynomial subtract2Polynomial(Polynomial a);
};

class Data
{
	Polynomial* data; // Mang dong luu nhieu da thuc
};

