﻿#include "header.h"

Polynomial::Polynomial() {
	pHead = NULL;
}

Polynomial& Polynomial::operator=(Polynomial a) { // Tạo 1 linked list tương tự
	if (a.pHead != NULL) {
		this->pHead = new Node;
		this->pHead->pNext = NULL;
		int n = strlen(a.pHead->variable);
		this->pHead->variable = new char[n];
		this->pHead->index_number = new int[n];
		this->pHead->parameters = a.pHead->parameters;
		for (int i = 0; i < n; i++) {
			this->pHead->variable[i] = a.pHead->variable[i];
			this->pHead->index_number[i] = a.pHead->index_number[i];
		}
	}
	Node* cur = a.pHead->pNext;
	Node* cur2 = this->pHead;
	while (cur != NULL)
	{
		cur2->pNext = new Node;
		int n = strlen(cur->variable);
		cur2->pNext->parameters = cur->parameters;
		cur2->pNext->variable = new char[n];
		cur2->pNext->index_number = new int[n];
		for (int i = 0; i < n; i++) {
			cur2->pNext->variable[i] = cur->variable[i];
			cur2->pNext->index_number[i] = cur->index_number[i];
		}
		cur2 = cur2->pNext;
		cur2->pNext = NULL;
		cur = cur->pNext;
	}
	return *this;
}

Polynomial Polynomial::add2Polynomial(Polynomial a) {
	Polynomial cur1, cur2;
	cur1 = *this;
	cur2 = a;
	Node *cur = cur1.pHead;
	while (cur->pNext != NULL) cur = cur->pNext;
	cur->pNext = cur2.pHead; //Nối 2 dãy thành 1 dãy con
							 //Gọi hàm rút gọn 
							 //Gọi hàm chuẩn hóa
	return cur1;
}
Polynomial Polynomial::subtract2Polynomial(Polynomial a) {
	Polynomial cur1, cur2;
	cur1 = *this;
	cur2 = a;
	Node *curr = cur2.pHead;
	while (curr != NULL) {// Đổi dấu đa thức bị trừ
		curr->parameters *= (-1);
		curr = curr->pNext;
	}
	Node *cur = cur1.pHead;
	while (cur->pNext != NULL) cur = cur->pNext;
	cur->pNext = cur2.pHead; //Nối 2 dãy thành 1 dãy con
							 //Gọi hàm rút gọn
							 //Gọi hàm chuẩn hóa
	return cur1;
}